'use strict';

const gulp = require('gulp');
const stylus = require('gulp-stylus');
const pug = require('gulp-pug');
const nib = require('./node_modules/nib');
const browserSync = require('browser-sync');
const gutil = require('gulp-util');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const plumber = require('gulp-plumber');
const svgSprite = require('gulp-svg-sprites');
const filter = require('gulp-filter');
const svg2png = require('gulp-svg2png');


gulp.task('sprites', function () {
    return gulp.src('./icons/source/**/*.svg')
        .pipe(plumber())
        .pipe(svgSprite({
            svg: {
                sprite: 'img/spritesheet.svg'
            },
            cssFile: 'style/icons.styl'
        }))
        .pipe(gulp.dest('icons/generated'))
        .pipe(filter('**/*.svg'))
        .pipe(svg2png())
        .pipe(gulp.dest('icons/generated'))
});


gulp.task('jsUglify', function() {
    gulp.src('./source/js/custom.js')
        .pipe(uglify())
        .pipe(gulp.dest('./public/js'))
        .pipe(browserSync.reload({stream: true}))
});


gulp.task('imageMin', function() {
    gulp.src('./source/images/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img'))
});

gulp.task('imageMinTemp', function() {
    gulp.src('./source/images/temp/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img/temp'))
});

gulp.task('imageMinBrands', function() {
    gulp.src('./source/images/brands/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img/brands'))
});

gulp.task('imageMinPartners', function() {
    gulp.src('./source/images/partners/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img/partners'))
});


gulp.task('styles', function() {
    return gulp.src('./source/style/style.styl')
    .pipe(stylus({
        use: nib(),
        compress: true
    }))
    .pipe(plumber())
    .pipe(gulp.dest('./public/css'))
    .pipe(browserSync.reload({stream: true}))
});


gulp.task('pages', function(){
	return gulp.src('./source/pug/pages/*.pug')
	.pipe(pug({pretty: false}))
	.pipe(gulp.dest('public'))
	.pipe(browserSync.reload({stream: true}))
});


gulp.task('browser-sync', function() { 
	browserSync({ 
		server: { 
			baseDir: 'public' 
		},
		notify: false,
		open: false
	});
});

gulp.task('watch', function() {
	gulp.watch(['./source/style/style.styl', './source/style/**/*.styl'], ['styles'])
	gulp.watch('./source/**/*.pug', ['pages'])
	gulp.watch('./source/js/custom.js', ['jsUglify'])
});

gulp.task('default', ['browser-sync', 'pages', 'styles', 'watch']); 