$(function() {

    // front slider

    $('.js-front-slider').owlCarousel({
        items: 1,
        nav: true,
        loop: true,
        mouseDrag: false
    });

    //select style

    $(".form-select").chosen({
        disable_search: true,
        width: "100%"
    });

    $('.chosen-drop').addClass('animated fadeIn');

    // change catalog colors

    $('.catalog-list__item-colors-button').click(function() {
        $(this).addClass('active').siblings('.catalog-list__item-colors-button').removeClass('active');
    });


    // products list scroll

    $('.front-content__products').each(function() {
        var api = $(this).find('.js-product-scroll').jScrollPane({ autoReinitialise: true }).data('jsp');
        $(this).find('.js-product-prev').bind('click', function() {
            api.scrollByX(-304);
            return false;
        });
        $(this).find('.js-product-next').bind('click', function() {
            api.scrollByX(304);
            return false;
        });
    });

    $('.front-content__products').each(function() {
        var api = $(this).find('.js-product-scroll-1').jScrollPane({ autoReinitialise: true }).data('jsp');
        $(this).find('.js-product-prev-1').bind('click', function() {
            api.scrollByX(-304);
            return false;
        });
        $(this).find('.js-product-next-1').bind('click', function() {
            api.scrollByX(304);
            return false;
        });
    });

    $('.front-content__products').each(function() {
        var api = $(this).find('.js-product-scroll-2').jScrollPane({ autoReinitialise: true }).data('jsp');
        $(this).find('.js-product-prev-2').bind('click', function() {
            api.scrollByX(-304);
            return false;
        });
        $(this).find('.js-product-next-2').bind('click', function() {
            api.scrollByX(304);
            return false;
        });
    });

    $('.front-content__products').each(function() {
        var api = $(this).find('.js-product-scroll-3').jScrollPane({ autoReinitialise: true }).data('jsp');
        $(this).find('.js-product-prev-3').bind('click', function() {
            api.scrollByX(-304);
            return false;
        });
        $(this).find('.js-product-next-3').bind('click', function() {
            api.scrollByX(304);
            return false;
        });
    });

    $('.front-content__products').each(function() {
        var api = $(this).find('.js-product-scroll-4').jScrollPane({ autoReinitialise: true }).data('jsp');
        $(this).find('.js-product-prev-4').bind('click', function() {
            api.scrollByX(-304);
            return false;
        });
        $(this).find('.js-product-next-4').bind('click', function() {
            api.scrollByX(304);
            return false;
        });
    });

    $('.front-content__products').each(function() {
        var api = $(this).find('.js-product-scroll-5').jScrollPane({ autoReinitialise: true }).data('jsp');
        $(this).find('.js-product-prev-5').bind('click', function() {
            api.scrollByX(-304);
            return false;
        });
        $(this).find('.js-product-next-5').bind('click', function() {
            api.scrollByX(304);
            return false;
        });
    });


    enquire.register("screen and (max-width: 1249px)", {

        deferSetup: true,

        match: function() {

            $('.front-content__products').each(function() {
                var api = $(this).find('.js-product-scroll').jScrollPane({ autoReinitialise: true }).data('jsp');
                $(this).find('.js-product-prev').bind('click', function() {
                    api.scrollByX(-246);
                    return false;
                });
                $(this).find('.js-product-next').bind('click', function() {
                    api.scrollByX(246);
                    return false;
                });
            });

            $('.front-content__products').each(function() {
                var api = $(this).find('.js-product-scroll-1').jScrollPane({ autoReinitialise: true }).data('jsp');
                $(this).find('.js-product-prev-1').bind('click', function() {
                    api.scrollByX(-246);
                    return false;
                });
                $(this).find('.js-product-next-1').bind('click', function() {
                    api.scrollByX(246);
                    return false;
                });
            });

            $('.front-content__products').each(function() {
                var api = $(this).find('.js-product-scroll-2').jScrollPane({ autoReinitialise: true }).data('jsp');
                $(this).find('.js-product-prev-2').bind('click', function() {
                    api.scrollByX(-246);
                    return false;
                });
                $(this).find('.js-product-next-2').bind('click', function() {
                    api.scrollByX(246);
                    return false;
                });
            });

            $('.front-content__products').each(function() {
                var api = $(this).find('.js-product-scroll-3').jScrollPane({ autoReinitialise: true }).data('jsp');
                $(this).find('.js-product-prev-3').bind('click', function() {
                    api.scrollByX(-246);
                    return false;
                });
                $(this).find('.js-product-next-3').bind('click', function() {
                    api.scrollByX(246);
                    return false;
                });
            });

            $('.front-content__products').each(function() {
                var api = $(this).find('.js-product-scroll-4').jScrollPane({ autoReinitialise: true }).data('jsp');
                $(this).find('.js-product-prev-4').bind('click', function() {
                    api.scrollByX(-246);
                    return false;
                });
                $(this).find('.js-product-next-4').bind('click', function() {
                    api.scrollByX(246);
                    return false;
                });
            });

            $('.front-content__products').each(function() {
                var api = $(this).find('.js-product-scroll-5').jScrollPane({ autoReinitialise: true }).data('jsp');
                $(this).find('.js-product-prev-5').bind('click', function() {
                    api.scrollByX(-246);
                    return false;
                });
                $(this).find('.js-product-next-5').bind('click', function() {
                    api.scrollByX(246);
                    return false;
                });
            });

        },
        unmatch: function() {

        }

    });








    // product list tabs

    $('.front-content__products-tabs').delegate('.front-content__products-tabs-tab:not(.active)', 'click', function() {
        $(this).addClass('active').siblings().removeClass('active').closest('.front-content__products').find('.front-content__products-content-wrapper').eq($(this).index()).addClass('active').siblings('.front-content__products-content-wrapper').removeClass('active');
        $(this).closest('.front-content__products').find('.front-content__products-top-buttons').eq($(this).index()).addClass('active').siblings('.front-content__products-top-buttons').removeClass('active');
    });


    // open bottom text

    $('.js-open-bottom-text').click(function() {
        $(this).closest('.bottom-text__content').toggleClass('active');
    });

    // filter range

    $('.js-price-range').noUiSlider({
        start: [100, 50000],
        connect: true,
        range: {
            'min': 100,
            'max': 50000
        },
        format: wNumb({
            mark: ',',
            decimals: 0
        }),
    });
    $('.js-price-range').Link('lower').to($('#min-price'));
    $('.js-price-range').Link('upper').to($('#max-price'));

    $('.js-price-range-2').noUiSlider({
        start: [100, 50000],
        connect: true,
        range: {
            'min': 100,
            'max': 50000
        },
        format: wNumb({
            mark: ',',
            decimals: 0
        }),
    });
    $('.js-price-range-2').Link('lower').to($('#min-price-2'));
    $('.js-price-range-2').Link('upper').to($('#max-price-2'));

    // sidebar filter link action 

    $('.sidebar-filter__link').click(function() {
        $(this).toggleClass('active');
    });

    // sidebar filter tabs 

    $('.sidebar-filter__tabs-button').click(function() {
        $(this).addClass('active').siblings('.sidebar-filter__tabs-button').removeClass('active');
    });


    // sidebar filter scroll

    $('.js-filter-scroll').each(function() {
        var pane = $(this);
        pane.jScrollPane({
            autoReinitialise: true
        });
    });

    // sidebar filter toggle sections

    $('.sidebar-filter__title').click(function() {
        $(this).closest('.sidebar-filter__section').toggleClass('collapsed');
    });

    // sidebar filter open toggle

    $('.js-toggle-filter-button').click(function() {
        $(this).toggleClass('active');
        $('.sidebar-filter').toggleClass('active');
    });

    // filter list collapsed

    $('.sidebar-filter__list').each(function () {
        if ($(this).find('.sidebar-filter__link').length > 9) {
            $(this).addClass('more-links');
        }
    });

    $('.sidebar-filter__list.more-links').addClass('collapsed');

    $('.sidebar-filter__view-more').click(function () {
        $(this).prev('.sidebar-filter__list').toggleClass('collapsed');
    });

    // product zoom gallery

    $(".js-zoom-img").elevateZoom({
        gallery: 'zoom-thumbs',
        cursor: 'pointer',
        galleryActiveClass: 'active',
        imageCrossfade: true,
        responsive: true
    });

    $('.js-video-button').click(function() {
        $('.product-main__gallery-video').addClass('active');
        $('.zoomContainer').addClass('no-display');
    });

    $('.product-main__gallery').closest('body').addClass('show-zoom');

    $('.js-video-button').closest('body').removeClass('show-zoom');

    $('.js-pr-img-button').click(function() {
        $('.product-main__gallery-video').removeClass('active');
        $('.zoomContainer').removeClass('no-display');
        $('body').addClass('show-zoom');
    });

    // product info tabs

    $('.product-bottom__tabs').delegate('.product-bottom__tabs-button:not(.active)', 'click', function() {
        $(this).addClass('active').siblings().removeClass('active').closest('.product-bottom').find('.js-product-info-block').eq($(this).index()).addClass('active').siblings('.js-product-info-block').removeClass('active');
    });

    // remove product in cart

    $(".cart-table__delete").click(function() {
        $(this).closest("tr").fadeOut(200, function() {
            $(this).remove();
        })
        return false;
    });

    // product gallery carousel

    $('.product-main__gallery-thumbs').each(function(index, element) {
        if ($(this).find('> a').length > 6) {
            $(this).bxSlider({
                hideControlOnEnd: true,
                infiniteLoop: false,
                pager: false,
                auto: false,
                minSlides: 6,
                maxSlides: 6,
                moveSlides: 1,
                mode: 'vertical',
                slideMargin: 10
            });
        }
    });

    // faq list open

    $(".faq-content__question").click(function() {
        $(this).closest(".faq-content__item").toggleClass('active').siblings('.faq-content__item').removeClass('active');
    });

    // modal colors scroll

    $('.js-modal-colors-content').each(function() {
        var pane = $(this);
        pane.bind(
            'jsp-scroll-y',
            function(event, scrollPositionY, isAtTop, isAtBottom) {
                if (isAtBottom === true) {
                    $(this).addClass('in-bottom');
                } else {
                    $(this).removeClass('in-bottom');
                }

            }
        );
        pane.jScrollPane({
            autoReinitialise: true
        });

    });

    // reviews rating

    $('.js-rating-stars').barrating({
        showSelectedRating: false
    });

    // open modal colors

    $('.more-colors-panel').click(function(event) {
        event.stopPropagation();
    });

    $(".js-colors-modal-button").click(function() {
        $(".js-modal-colors").addClass('active');
        return false;
    });


    // popup gallery slider

    $('.js-popup-gallery-slider').each(function(index, element) {
        if ($(this).find('> img').length > 1) {
            $(this).bxSlider({
                pagerCustom: '.js-popup-thumbs-slider',
                mode: 'fade',
                slideWidth: 650
            });
        }
    });

    $('.js-popup-thumbs-slider').each(function(index, element) {
        if ($(this).find('> a').length > 6) {
            $(this).bxSlider({
                hideControlOnEnd: true,
                infiniteLoop: false,
                pager: false,
                auto: false,
                minSlides: 6,
                maxSlides: 6,
                moveSlides: 1,
                mode: 'vertical',
                slideMargin: 10
            });
        }
    });

    // open modal gallery

    $('.gallery-popup').click(function(event) {
        event.stopPropagation();
    });

    $(".js-open-gallery-button").click(function() {
        $(".js-popup-gallery").addClass('active');
        return false;
    });

    // open callback panel

    $('.modal-form-panel').click(function(event) {
        event.stopPropagation();
    });

    $(".js-callback-button").click(function() {
        $(".js-callback-panel").addClass('active');
        return false;
    });

    // open write manager panel

    $(".js-write-manager-button").click(function() {
        $(".js-write-manager-panel").addClass('active');
        return false;
    });

    // open add review panel

    $(".js-add-review-button").click(function() {
        $(".js-add-review-panel").addClass('active');
        return false;
    });



    // closed modal panels


    $(".js-closed-modal-panel").click(function() {
        $(this).closest('.overlay-content').removeClass('active');
    });

    $("body").click(function() {
        $('.overlay-content').removeClass('active');
    });

    // toggle added cart

    $('.js-added-cart-panel').click(function(event) {
        event.stopPropagation();
    });

    $(".js-added-cart-button").click(function() {
        $(".js-added-cart-panel").addClass('active');
        $('body').addClass('content-overlay');
        return false;
    });

    $(".js-added-cart-closed, body").click(function() {
        $(".js-added-cart-panel").removeClass('active');
        $('body').removeClass('content-overlay');
    });

    // header search

    $(".header-bottom__search-text").focus(function() {
        $('.header-bottom').addClass('search-view');
    });

    $(".js-closed-search-button").click(function() {
        $('.header-bottom').removeClass('search-view');
        $('.header-layout').removeClass('open-search')
    });

    $('.js-small-search-button').click(function() {
        $('.header-layout').toggleClass('open-search')
        $('.header-bottom').toggleClass('search-view');
    });

    // footer small open

    $('.footer-content__title').click(function() {
        $(this).closest('.footer-content__column').toggleClass('active');
    });

    // product medium gallery

    $('.js-medium-product-gallery-thumbs').each(function(index, element) {
        if ($(this).find('> a').length > 5) {
            $(this).bxSlider({
                hideControlOnEnd: true,
                infiniteLoop: false,
                pager: false,
                auto: false,
                minSlides: 5,
                maxSlides: 5,
                moveSlides: 1,
                mode: 'vertical',
                slideMargin: 10
            });
        }
    });



    enquire.register("screen and (max-width: 1249px) and (min-width: 768px)", {

        deferSetup: true,

        match: function() {

            $('.js-medium-product-gallery-content').bxSlider({
                pagerCustom: '.js-medium-product-gallery-thumbs',
                mode: 'fade',
                controls: false
            });


        },
        unmatch: function() {}

    });

    enquire.register("screen and (max-width: 767px)", {

        deferSetup: true,

        match: function() {

            $('.js-medium-product-gallery-content').bxSlider({
                pager: true,
                controls: false
            });

        },
        unmatch: function() {}

    });


    // medium filter

    $('.js-medium-filter-button').click(function() {
        $(this).closest('.medium-filter-button').addClass('active');
        $('.js-medium-filter').addClass('active');
    });

    $('.medium-filter__closed').click(function() {
        $('.medium-filter-button, .js-medium-filter').removeClass('active');
    });

    $('.medium-filter__section-button').click(function() {
        $(this).closest('.medium-filter__section').toggleClass('active');
    });

    // medium menu

    $('.js-burger-button').click(function() {
        $(this).toggleClass('active');
        $('body').toggleClass('no-scroll');
        $('.js-medium-menu').toggleClass('active');
        $('.medium-filter-button, .js-medium-filter').toggleClass('menu-mode');
        $('.js-medium-filter').removeClass('animated');
        $('.medium-menu__menu').removeClass('hidden-parents')
        $('.medium-menu__menu-item').removeClass('active');
    });

    $('.js-medium-menu-button').click(function() {
        $(this).closest('.medium-menu__menu').addClass('hidden-parents')
        $(this).closest('.medium-menu__menu-item').addClass('active').siblings('.medium-menu__menu-item').removeClass('active');
    });

    $('.js-medium-menu-back').click(function() {
        $(this).closest('.medium-menu__menu').removeClass('hidden-parents')
        $('.medium-menu__menu-item').removeClass('active');
    });

    // toggle header fixed

    $(window).scroll(function() {
        if ($(this).scrollTop() > 134) {
            $('.header-layout').addClass('fixed-mode');
            $('.layout-body').addClass('fixed-header');
        } else {
            $('.header-layout').removeClass('fixed-mode');
            $('.layout-body').removeClass('fixed-header');
        }
    });

    // fixed cart sidebar


    enquire.register("screen and (min-width: 1249px)", {

        deferSetup: true,

        match: function() {

            $('.cart-container').each(function() {
                var a = document.querySelector('#cart-actions'),
                    b = null,
                    P = 80;
                window.addEventListener('scroll', Ascroll, false);
                document.body.addEventListener('scroll', Ascroll, false);

                function Ascroll() {
                    if (b == null) {
                        var Sa = getComputedStyle(a, ''),
                            s = '';
                        for (var i = 0; i < Sa.length; i++) {
                            if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                                s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                            }
                        }
                        b = document.createElement('div');
                        b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                        a.insertBefore(b, a.firstChild);
                        var l = a.childNodes.length;
                        for (var i = 1; i < l; i++) {
                            b.appendChild(a.childNodes[1]);
                        }
                        a.style.height = b.getBoundingClientRect().height + 'px';
                        a.style.padding = '0';
                        a.style.border = '0';
                    }
                    var Ra = a.getBoundingClientRect(),
                        R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('#other-products').getBoundingClientRect().top + 0);
                    if ((Ra.top - P) <= 0) {
                        if ((Ra.top - P) <= R) {
                            b.className = 'stop-sidebar';
                            b.style.top = -R + 'px';
                        } else {
                            b.className = 'sticky-sidebar';
                            b.style.top = P + 'px';
                        }
                    } else {
                        b.className = '';
                        b.style.top = '';
                    }
                    window.addEventListener('resize', function() {
                        a.children[0].style.width = getComputedStyle(a, '').width
                    }, false);
                }
            });


        },
        unmatch: function() {}

    });


    // front logo slider

    $('.front-content__brands').each(function() {
        var owl = $(this).find('.js-logo-list-slider');
        owl.owlCarousel({
            margin: 50,
            loop: true,
            autoWidth: true,
            items: 8
        });
        // Go to the next item
        $(this).find('.js-logo-next').click(function() {
                owl.trigger('next.owl.carousel');
            })
            // Go to the previous item
        $(this).find('.js-logo-prev').click(function() {
            owl.trigger('prev.owl.carousel');
        })
    });

    $('.front-content__partners').each(function() {
        var owl = $(this).find('.js-logo-list-slider');
        owl.owlCarousel({
            margin: 65,
            loop: true,
            autoWidth: true,
            items: 6
        });
        // Go to the next item
        $(this).find('.js-logo-next').click(function() {
                owl.trigger('next.owl.carousel');
            })
            // Go to the previous item
        $(this).find('.js-logo-prev').click(function() {
            owl.trigger('prev.owl.carousel');
        })
    });

    // tels dropdowm

    $('.header-top__tels').click(function(event) {
        event.stopPropagation();
    });

    $('.header-top__tels-active').click(function() {
        $(this).closest('.header-top__tels').toggleClass('active');
        return false;
    });

    $('body').click(function(event) {
        $('.header-top__tels').removeClass('active');
    });

    // open overlays 

    $('.js-parent-menu, .header-bottom__catalog').hover(
        function() {
            $(this).closest('body').addClass('content-overlay');
        },
        function() {
            $(this).closest('body').removeClass('content-overlay');
        }
    );


    // hover anonce product

    $('.front-content__products .catalog-list').hover(
        function() {
            $(this).closest('.front-content__products').addClass('hover-mode');
        },
        function() {
            $(this).closest('.front-content__products').removeClass('hover-mode');
        }
    );

    // product colors slider

    $('.js-product-color-slider').each(function(index, element) {
        if ($(this).find('.product-main__colors-button').length > 9) {
            $(this).addClass('owl-carousel');
            $(this).owlCarousel({
                items: 9,
                nav: true,
                dots: false,
                loop: false,
                mouseDrag: false,
                responsive: {
                    0: {
                        items: 7
                    },
                    1249: {
                        items: 9
                    }
                }
            });
        }
    });

    // colors tooltip

    $('.product-main__colors-button').tooltipster({
        animation: 'grow',
        theme: 'tooltipster-pink'
    });

    // button Up

    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.js-button-up').addClass('active');
        } else {
            $('.js-button-up').removeClass('active');
        }
    });

    $('.js-button-up').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    // catalog thumbs slider

    $('.catalog-list__item-thumbs').each(function(index, element) {
        if ($(this).find('.catalog-list__item-thumbs-button').length > 4) {
            $(this).addClass('owl-carousel');
            $(this).owlCarousel({
                items: 4,
                nav: true,
                dots: false,
                loop: false,
                mouseDrag: false
            });
        }
    });

    // change catalog thumbs

    $('.catalog-list__item-thumbs:not(.owl-carousel)').each(function(index, element) {
        $(this).find('.catalog-list__item-thumbs-button').click(function() {
            $(this).addClass('active').siblings('.catalog-list__item-thumbs-button').removeClass('active');
        });
    });

    $('.catalog-list__item-thumbs.owl-carousel').each(function(index, element) {
        $(this).find('.catalog-list__item-thumbs-button').click(function() {
            $(this).addClass('active').closest('.owl-item').siblings('.owl-item').find('.catalog-list__item-thumbs-button').removeClass('active');
        });
    });

    // phone mask

    $('#phone').mask('+38(099) 999-9999');

    // sidebar tooltip

    $('.sidebar-filter__tooltip').hover(function () {
        var obj = $(this);
        var left = obj.offset().left;
        var top = obj.offset().top;
        var posTop = top - $(window).scrollTop();
        var tooltip = obj.find('.sidebar-filter__tooltip-text').clone();
        tooltip.appendTo('body');
        tooltip.css({
            top: posTop,
            left: left
        });
    }, function () {
        $('body > .sidebar-filter__tooltip-text').remove();
    });

    // choice city

    $('.header-top__city-button').click(function() {
        $('.popup-city').addClass('active');
    });

    $('.header-top').each(function() {
        if ($('.popup-city__list-text').hasClass('active')) {
            var cityActive = $('.popup-city__list-text.active').text();
            $('.header-top__city-button').text(cityActive);
            $('.popup-city__search-field').val(cityActive);
        }
    });

    $('.popup-city__list-text').click(function () {
        var title = $(this).text();
        $(this).addClass('active');
        $(this).closest('.popup-city__list-item').siblings('.popup-city__list-item').find('.popup-city__list-text').removeClass('active');
        $(this).closest('.popup-city').find('.popup-city__search-field').val(title);
        $('.header-top__city-button').text(title);
        $('.popup-city').removeClass('active');
        return false;
    });

    $('.popup-city, .header-top__city-button').click(function(event) {
        event.stopPropagation();
    });

    $('.popup-city__closed, body').click(function() {
        $('.popup-city').removeClass('active');
    });

});




// closed old panel

function closeOldPanel() {
    var elem = document.getElementsByClassName('go-old-panel');
    elem[0].classList.add('no-display');
}
